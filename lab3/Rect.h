
#ifndef LAB1_RECT_H
#define LAB1_RECT_H

#include "Line.h"

class Rect {
public:
    Rect(CuttingRect cuttingRect);

    void draw();

private:
    Line first;
    Line second;
    Line third;
    Line fourth;
};


#endif //LAB1_RECT_H
