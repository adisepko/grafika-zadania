#include "Line.h"
#include <allegro.h>
#include <bitset>
#include "../common/common.h"

char calculate_code(const CuttingRect &rect, Point point);

Line::Line(CuttingRect rect) {
    first_point = Point();
    int rect_length = rect.get_x_max() - rect.get_x_min();
    int rect_height = rect.get_y_max() - rect.get_y_min();
    int x_distance = (first_point.get_x() + rect_length) > screen->w ? screen->w : first_point.get_x() + rect_length;
    int y_distance = (first_point.get_y() + rect_height) > screen->h ? screen->h : first_point.get_y() + rect_height;
    second_point = Point(first_point.get_x(), x_distance, first_point.get_y(), y_distance);
    first_points_code = calculate_code(rect, first_point);
    second_points_code = calculate_code(rect, second_point);
    this->rect = rect;
}

Line::Line(CuttingRect rect, Point first_point, Point second_point) {
    this->first_point = first_point;
    this->second_point = second_point;
    first_points_code = calculate_code(rect, first_point);
    second_points_code = calculate_code(rect, second_point);
    this->rect = rect;
}

Line::Line(CuttingRect rect, Point first_point) {
    this->first_point = first_point;
    int rect_length = rect.get_x_max() - rect.get_x_min();
    int rect_height = rect.get_y_max() - rect.get_y_min();
    int x_distance = (first_point.get_x() + rect_length) > screen->w ? screen->w : first_point.get_x() + rect_length;
    int y_distance = (first_point.get_y() + rect_height) > screen->h ? screen->h : first_point.get_y() + rect_height;
    second_point = Point(first_point.get_x(), x_distance, first_point.get_y(), y_distance);
    first_points_code = calculate_code(rect, first_point);
    second_points_code = calculate_code(rect, second_point);
    this->rect = rect;
}

char calculate_code(const CuttingRect &rect, Point point) {
    char code = 0;
    if (point.get_x() < rect.get_x_min()) {
        code = code | 1;
    } else if (point.get_x() > rect.get_x_max()) {
        code = code | 2;
    }
    if (point.get_y() > rect.get_y_max()) {
        code = code | 4;
    } else if (point.get_y() < rect.get_y_min()) {
        code = code | 8;
    }
    std::bitset<8> x(code);
    std::cout << "code: " << x << std::endl;
    return code;
}

void Line::draw() {
    if (first_points_code == 0 && second_points_code == 0) {
        line(screen, first_point.get_x(), first_point.get_y(),
             second_point.get_x(), second_point.get_y(), black);
    } else if ((first_points_code & second_points_code) != 0) {
        line(screen, first_point.get_x(), first_point.get_y(),
             second_point.get_x(), second_point.get_y(), makecol(255, 0, 0));
    } else {
        line(screen, first_point.get_x(), first_point.get_y(),
             second_point.get_x(), second_point.get_y(), makecol(255, 0, 0));
        while (first_points_code != second_points_code) {
            if (first_points_code == 0) {
                swap();
            }
            int x1 = first_point.get_x();
            int y1 = first_point.get_y();
            if (first_points_code & 1) {
                y1 = y1 + (rect.get_x_min() - x1) * (second_point.get_y() - y1) / (second_point.get_x() - x1);
                x1 = rect.get_x_min();
                set_first_point(x1, y1);
                continue;
            } else if (first_points_code & 2) {
                y1 = y1 + (rect.get_x_max() - x1) * (second_point.get_y() - y1) / (second_point.get_x() - x1);
                x1 = rect.get_x_max();
                set_first_point(x1, y1);
                continue;
            }

            if (first_points_code & 4) {
                x1 = x1 + (rect.get_y_max() - y1) * (second_point.get_x() - x1) / (second_point.get_y() - y1);
                y1 = rect.get_y_max();
                set_first_point(x1, y1);
                continue;
            } else if (first_points_code & 8) {
                x1 = x1 + (rect.get_y_min() - y1) * (second_point.get_x() - x1) / (second_point.get_y() - y1);
                y1 = rect.get_y_min();
                set_first_point(x1, y1);
                continue;
            }
        }
        line(screen, first_point.get_x(), first_point.get_y(),
             second_point.get_x(), second_point.get_y(), makecol(0, 255, 0));
    }
}

void Line::set_first_point(int x1, int y1) {
    first_point = Point(x1, y1);
    first_points_code = calculate_code(rect, first_point);
    second_points_code = calculate_code(rect, second_point);
}

void Line::swap() {
    Point tmp = first_point;
    first_point = second_point;
    second_point = tmp;
    char tmp_code = first_points_code;
    first_points_code = second_points_code;
    second_points_code = tmp_code;

}

Point Line::get_first_point() const {
    return first_point;
}

Point Line::get_second_point() const {
    return second_point;
}
