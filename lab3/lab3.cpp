#include "../common/common.h"
#include "CuttingRect.h"
#include "Line.h"
#include "Triangle.h"
#include "Rect.h"

void lab3() {
    clear_screen();
    do {
        clear_screen();
        int width = get_random_int_in_range(300, 600);
        int height = get_random_int_in_range(200, width);
        CuttingRect cutting_rect(width, height);
        cutting_rect.draw();
        Line first_line(cutting_rect);
        Line second_line(cutting_rect);
        first_line.draw();
        second_line.draw();
        Triangle triangle(cutting_rect);
        triangle.draw();
        Rect rectangle(cutting_rect);
        rectangle.draw();
    } while ((readkey() >> 8) != KEY_ESC);
    wait_on_esc();
}