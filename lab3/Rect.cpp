
#include "Rect.h"

Rect::Rect(CuttingRect cuttingRect) {
    Line diagonal = Line(cuttingRect);
    Point p1 = diagonal.get_first_point();
    Point p2 = diagonal.get_second_point();
    first = Line(cuttingRect, Point(p1.get_x(), p1.get_y()), Point(p2.get_x(), p1.get_y()));
    second = Line(cuttingRect, Point(p1.get_x(), p1.get_y()), Point(p1.get_x(), p2.get_y()));
    third = Line(cuttingRect, Point(p1.get_x(), p2.get_y()), Point(p2.get_x(), p2.get_y()));
    fourth = Line(cuttingRect, Point(p2.get_x(), p1.get_y()), Point(p2.get_x(), p2.get_y()));
}

void Rect::draw() {
    first.draw();
    second.draw();
    third.draw();
    fourth.draw();
}
