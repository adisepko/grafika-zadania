
#include "Point.h"
#include "../common/common.h"
#include <allegro.h>

Point::Point() {
    x = get_random_int_in_range(0, screen->w);
    y = get_random_int_in_range(0, screen->h);
}

Point::Point(int x, int x_distance, int y, int y_distance) {
    this->x = get_random_int_in_range(x, x_distance);
    this->y = get_random_int_in_range(y, y_distance);
}

Point::Point(int x, int y) {
    this->x = x;
    this->y = y;
}

int Point::get_x() const {
    return x;
}

int Point::get_y() const {
    return y;
}