#ifndef LAB1_CUTTINGRECT_H
#define LAB1_CUTTINGRECT_H


class CuttingRect {
public:
    CuttingRect();

    CuttingRect(int length, int height);

    void draw() const;

    int get_x_min() const;

    int get_x_max() const;

    int get_y_min() const;

    int get_y_max() const;

private:
    int x_min;
    int y_min;
    int x_max;
    int y_max;
};


#endif //LAB1_CUTTINGRECT_H
