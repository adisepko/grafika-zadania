#include "Triangle.h"

Triangle::Triangle(CuttingRect rect) {
    first = Line(rect);
    second = Line(rect, first.get_first_point());
    third = Line(rect, first.get_second_point(), second.get_second_point());
}

void Triangle::draw() {
    first.draw();
    second.draw();
    third.draw();
}
