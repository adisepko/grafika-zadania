#include "CuttingRect.h"
#include <allegro.h>
#include "../common/common.h"

CuttingRect::CuttingRect(int length, int height) {
    x_min = screen->w / 2 - length / 2;
    x_max = screen->w / 2 + length / 2;
    y_min = screen->h / 2 - height / 2;
    y_max = screen->h / 2 + height / 2;
}

CuttingRect::CuttingRect() {
    x_min = screen->w / 2 - 100 / 2;
    x_max = screen->w / 2 + 100 / 2;
    y_min = screen->h / 2 - 100 / 2;
    y_max = screen->h / 2 + 100 / 2;
}

void CuttingRect::draw() const {
    rect(screen, x_min, y_min, x_max, y_max, black);
}

int CuttingRect::get_x_min() const { return x_min; }

int CuttingRect::get_x_max() const { return x_max; }

int CuttingRect::get_y_min() const { return y_min; }

int CuttingRect::get_y_max() const { return y_max; }