
#ifndef LAB1_LINE_H
#define LAB1_LINE_H

#include "Point.h"
#include "CuttingRect.h"

class Line {
public:
    Line() = default;

    Line(CuttingRect rect);

    Line(CuttingRect rect, Point first_point, Point second_point);

    Line(CuttingRect rect, Point first_point);

    void draw();

    Point get_first_point() const;

    Point get_second_point() const;

private:
    CuttingRect rect;
    Point first_point;
    char first_points_code;
    Point second_point;
    char second_points_code;


    void swap();

    void set_first_point(int x1, int y1);
};


#endif //LAB1_LINE_H
