#ifndef LAB1_POINT_H
#define LAB1_POINT_H


class Point {
public:
    Point();

    Point(int x, int x_distance, int y, int y_distance);

    Point(int x, int y);

    int get_x() const;

    int get_y() const;

private:
    int x;
    int y;
};


#endif //LAB1_POINT_H
