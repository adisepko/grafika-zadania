#ifndef LAB1_TRIANGLE_H
#define LAB1_TRIANGLE_H

#include "Line.h"

class Triangle {
public:
    Triangle(CuttingRect rect);

    void draw();

private:
    Line first;
    Line second;
    Line third;

};


#endif //LAB1_TRIANGLE_H
