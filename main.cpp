#include <allegro.h>
#include "lab1/lab1.h"
#include "lab2/lab2.h"
#include "lab3/lab3.h"
#include "lab4/lab4.h"
#include "common/common.h"

static const int SCREEN_WIDTH = 800;

static const int SCREEN_HEIGHT = 600;

int main() {
    allegro_init();
    install_mouse();
    install_keyboard();
    set_color_depth(8);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
    set_palette(default_palette);

    while (true) {
        clear_screen();
        textout_ex(screen, font, "Wybierz numer laboratorium", 20, 20, black, -1);
        int pressed_key = readkey() >> 8;
        if (pressed_key == KEY_1) {
            lab1();
        } else if (pressed_key == KEY_2) {
            lab2();
        } else if (pressed_key == KEY_3) {
            lab3();
        } else if (pressed_key == KEY_4) {
            lab4();
        } else if (pressed_key == KEY_ESC) {
            break;
        }

    }

    allegro_exit();
    return 0;
}

END_OF_MAIN();