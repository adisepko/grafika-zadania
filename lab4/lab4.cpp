#include <cmath>
#include <queue>
#include <allegro.h>
#include "../common/common.h"

# define M_PIl          3.141592653589793238462643383279502884L


struct Point {
    int x;
    int y;
};

void draw_circle();

void flood_fill(int x, int y, int old_color, int new_color);

void process_point(int x, int y, int old_color, int new_color, std::deque<Point> &points);

void lab4() {
    clear_screen();
    int circle_count = get_random_int_in_range(1, 10);
    int new_color = get_random_color();
    for (int i = 0; i < circle_count; ++i) {
        draw_circle();
    }
    do {
        if (mouse_b & 1) {
            int old_color = getpixel(screen, mouse_x, mouse_y);
            flood_fill(mouse_x, mouse_y, old_color, new_color);
        }
        if (mouse_b & 2) {
            new_color = get_random_color();
        }
    } while (!key[KEY_ESC]);
    readkey();
}

void draw_circle() {
    int r = get_random_int_in_range(50, 300);
    int x0 = get_random_int_in_range(r, screen->w - r);
    int y0 = get_random_int_in_range(r, screen->h - r);
    for (double a = 0; a < M_PIl / 2; a += (1.0 / r)) {
        double x = r * cos(a);
        double y = r * sin(a);
        putpixel(screen, x0 - x, y0 - y, black);
        putpixel(screen, x0 - x, y0 + y, black);
        putpixel(screen, x0 + x, y0 - y, black);
        putpixel(screen, x0 + x, y0 + y, black);
    }
}

void flood_fill(int x, int y, int old_color, int new_color) {
    std::deque<Point> points;
    points.push_back({x, y});
    putpixel(screen, x, y, new_color);
    while (!points.empty()) {
        Point &point = points.front();
        x = point.x;
        y = point.y;
        points.pop_front();
        process_point(x + 1, y, old_color, new_color, points);
        process_point(x, y + 1, old_color, new_color, points);
        process_point(x, y - 1, old_color, new_color, points);
        process_point(x - 1, y, old_color, new_color, points);
    }
}

void process_point(int x, int y, int old_color, int new_color, std::deque<Point> &points) {
    if (getpixel(screen, x, y) == old_color && getpixel(screen, x, y) != new_color) {
        putpixel(screen, x, y, new_color);
        points.push_back({x, y});
    }
}
