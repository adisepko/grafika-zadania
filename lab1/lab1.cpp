//
// Created by adrian.sepiol on 4/16/2022.
//
#include <allegro.h>
#include "ex2/ex2.h"
#include "ex3/ex3.h"
#include "../common/common.h"


void lab1() {
    while (true) {
        clear_screen();
        textout_ex(screen, font, "Wybierz numer ćwiczenia", 20, 20, black, -1);
        int pressed_key = readkey() >> 8;
        if (pressed_key == KEY_1) {
            ex2();
        } else if (pressed_key == KEY_2) {
            ex3();
        } else if (pressed_key == KEY_ESC) {
            break;
        }
    }
}


