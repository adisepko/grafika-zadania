//
// Created by adrian.sepiol on 4/16/2022.
//
#include <allegro.h>
#include <iostream>
#include <random>
#include "../../common/common.h"

int getColor();

void draw_random_line();

void draw_random_rect();

void draw_random_circle();

void draw_random_triangle();

void ex3() {
    clear_screen();
    do {
        clear_screen();
        draw_random_line();
        draw_random_rect();
        draw_random_circle();
        draw_random_triangle();
    } while ((readkey() >> 8) != KEY_ESC);
}

void draw_random_line() {
    line(screen,
         get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
         get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
         getColor());
}

void draw_random_rect() {
    rect(screen,
         get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
         get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
         getColor());
}

void draw_random_circle() {
    circle(screen, get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
           get_random_int_in_range(20, 100), getColor());
}

void draw_random_triangle() {
    triangle(screen,
             get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
             get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
             get_random_int_in_range(0, screen->w), get_random_int_in_range(0, screen->h),
             getColor());
}

int getColor() {
    return makecol(get_random_int_in_range(0, 255),
                   get_random_int_in_range(0, 255),
                   get_random_int_in_range(0, 255));
}

