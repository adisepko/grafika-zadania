//
// Created by adrian.sepiol on 4/16/2022.
//
#include <utility>
#include <allegro.h>
#include "../../common/common.h"

void draw_triangle(int triangle_size, int color);

void draw_rect(int rectangle_size, int color);

void draw_rect_from_line(int line_len, int color);

void draw_circle(int radius, int color);

std::pair<int, int> getCenter();

void ex2() {
    clear_screen();
    draw_triangle(50, makecol(255, 0, 0));
    draw_rect(150, makecol(0, 255, 0));
    draw_circle(130, makecol(0, 0, 255));
    draw_rect_from_line(200, makecol(0, 0, 0));
    wait_on_esc();
}

void draw_triangle(int triangle_size, int color) {
    const std::pair<int, int> &center = getCenter();
    triangle(screen,
             center.first, center.second - triangle_size,
             center.first - triangle_size, center.second + triangle_size,
             center.first + triangle_size, center.second + triangle_size,
             color);
}

void draw_rect(int rectangle_size, int color) {
    const std::pair<int, int> &center = getCenter();
    rect(screen,
         center.first - rectangle_size, center.second - rectangle_size,
         center.first + rectangle_size, center.second + rectangle_size,
         color);
}

void draw_rect_from_line(int line_len, int color) {
    const std::pair<int, int> &center = getCenter();
    line(screen,
         center.first + line_len, center.second + line_len,
         center.first + line_len, center.second - line_len,
         color);
    line(screen,
         center.first - line_len, center.second + line_len,
         center.first - line_len, center.second - line_len,
         color);
    line(screen,
         center.first + line_len, center.second + line_len,
         center.first - line_len, center.second + line_len,
         color);
    line(screen,
         center.first + line_len, center.second - line_len,
         center.first - line_len, center.second - line_len,
         color);
}

void draw_circle(int radius, int color) {
    const std::pair<int, int> &center = getCenter();
    circle(screen, center.first, center.second, radius, color);
}

std::pair<int, int> getCenter() {
    std::pair<int, int> center{screen->w / 2, screen->h / 2};
    return center;
}