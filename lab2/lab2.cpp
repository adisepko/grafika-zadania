//
// Created by adrian.sepiol on 4/17/2022.
//
#include <allegro.h>
#include <cmath>
#include "lab2.h"
#include "../common/common.h"
#include <bits/stdc++.h>

void ex1(int size);

void ex2(int size);

void ex3(int size);

void ex4(int size);

void draw_line(int x0, int y0, int x1, int y1, int color);

void swap_points(int &x0, int &x1, int &y0, int &y1);

void lab2() {
    while (true) {
        clear_screen();
        textout_ex(screen, font, "Wybierz numer ćwiczenia", 20, 20, black, -1);
        int pressed_key = readkey() >> 8;
        if (pressed_key == KEY_1) {
            ex1(50);
        } else if (pressed_key == KEY_2) {
            ex2(50);
        } else if (pressed_key == KEY_3) {
            ex3(100);
        } else if (pressed_key == KEY_4) {
            ex4(100);
        } else if (pressed_key == KEY_ESC) {
            break;
        }
    }
}

void ex1(int size) {
    clear_screen();
    int width = screen->w;
    int height = screen->h;
    int x = 0;
    int y = 0;

    while (x < width || y < height) {
        x += size;
        if (x < width) draw_line(x, 0, x, height, black);
        if (y < height) draw_line(0, y, width, y, black);
        y += size;
    }
    wait_on_esc();
}

void ex2(int size) {
    clear_screen();
    int width = screen->w;
    int height = screen->h;
    for (int y = 0; y < height; y += size) {
        draw_line(0, 0, width, y, black);
        draw_line(0, y, width, height, black);
    }
    wait_on_esc();
}

void ex3(int size) {
    clear_screen();
    int width = screen->w;
    int height = screen->h;
    for (int x = 0; x <= width; x += size) {
        draw_line(x, 0, width - x, height, black);
    }
    for (int y = 0; y <= height / 2; y += size) {
        draw_line(0, y, width, height - y, black);
    }
    wait_on_esc();
}

void ex4(int size) {
    clear_screen();
    int width = screen->w;
    int height = screen->h;
    for (int x = 0; x <= width; x += size) {
        draw_line(0, 0, x, height, black);
        draw_line(x, 0, width, height, black);
    }
    for (int y = 0; y <= height; y += size) {
        draw_line(0, 0, width, y, black);
        draw_line(0, y, width, height, black);
    }
    wait_on_esc();
}

void draw_line(int x0, int y0, int x1, int y1, int color) {
    double dx = x1 - x0;
    double dy = y1 - y0;
    double m = dy / dx;
    if (abs(m) > 1) {
        if (y0 > y1) swap_points(x0, x1, y0, y1);
        m = dx / dy;
        double x = x0;
        for (int y = y0; y <= y1; ++y) {
            putpixel(screen, lround(x), y, color);
            x += m;
        }
    } else {
        if (x0 > x1) swap_points(x0, x1, y0, y1);
        double y = y0;
        for (int x = x0; x <= x1; ++x) {
            putpixel(screen, x, lround(y), color);
            y += m;
        }
    }
}

void swap_points(int &x0, int &x1, int &y0, int &y1) {
    std::swap(x0, x1);
    std::swap(y0, y1);
}

