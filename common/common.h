#ifndef LAB1_COMMON_H
#define LAB1_COMMON_H

#include <random>
#include <iostream>
#include <allegro.h>
#include "../common/common.h"

#define black makecol(0,0,0)
#define white makecol(255,255,255)

void clear_screen();

void wait_on_esc();

int get_random_int_in_range(int from, int to);

int get_random_color();

#endif //LAB1_COMMON_H
