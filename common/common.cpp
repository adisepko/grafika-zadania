//
// Created by adrian.sepiol on 4/17/2022.
//
#include "../common/common.h"
#include <random>
#include <allegro.h>


void clear_screen() {
    clear_to_color(screen, white);
}

void wait_on_esc() {
    while ((readkey() >> 8) != KEY_ESC);
}

int get_random_int_in_range(int from, int to) {
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(from, to);
    return distr(generator);
}

int get_random_color() {
    return makecol(get_random_int_in_range(0, 255), get_random_int_in_range(0, 255), get_random_int_in_range(0, 255));
}

